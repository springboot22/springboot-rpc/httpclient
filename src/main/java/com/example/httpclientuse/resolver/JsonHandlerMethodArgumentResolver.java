package com.example.httpclientuse.resolver;

import com.alibaba.fastjson.JSONObject;
import org.springframework.core.MethodParameter;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * 由于String类型的参数只要名字对的上，就可以将值匹配上，所以下面将String类型的去掉
 * 需要关注的是其他的对象类型的数据
 * 通过参数名和参数类型转成对应的对象
 */
public class JsonHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return !parameter.getParameterType().isAssignableFrom(String.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        String parameterName = parameter.getParameterName();
        if (!StringUtils.hasLength(parameterName)) {
            return null;
        }
        Class<?> parameterType = parameter.getParameterType();
        return JSONObject.parseObject(webRequest.getParameter(parameterName), parameterType);
    }
}