package com.example.httpclientuse.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.httpclientuse.dto.FileUploadVO;
import com.example.httpclientuse.dto.UserRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class FileUploadController {

    /**
     * 方法1
     * 经实验，文件上传接口无法同时使用{@link RequestBody}注解，
     * 暂时的处理方式是通过参数解析器 {@link com.example.httpclientuse.resolver.JsonHandlerMethodArgumentResolver}进行处理
     * 同时也发现，对于参数列表中的HttpServletRequest参数，不会走自定义的参数解析器，应该是有一个处理顺序的原因
     *
     * @param files
     * @param userRequest
     * @param permission
     */
    @PostMapping("/upload/file")
    public void uploadFile(MultipartFile[] files, UserRequest userRequest, List<Integer> intList, String permission) {
        for (MultipartFile file : files) {
            System.out.println(file.getOriginalFilename());
        }
        System.out.println(intList);
        System.out.println(userRequest.toString());
        System.out.println(permission);
    }

    /**
     * 方法2
     * 也可以通过 {@link HttpServletRequest} 对象通过参数名来获取对应的数据，再通过类型转换，转换成目标类型的对象
     * @param files
     * @param request
     */
    @PostMapping("/upload/file1")
    public void uploadFile(MultipartFile[] files, HttpServletRequest request) {
        for (MultipartFile file : files) {
            System.out.println(file.getOriginalFilename());
        }
        List<Integer> intList = JSONObject.parseArray(request.getParameter("intList"),Integer.class);
        UserRequest userRequest = JSONObject.parseObject(request.getParameter("userRequest"), UserRequest.class);
        String permission = request.getParameter("permission");
        System.out.println(intList);
        System.out.println(userRequest);
        System.out.println(permission);
    }

    /**
     * 方法3
     * {@link RequestPart} 注解 这个注解用在multipart/form-data表单提交请求的方法上。
     * {@link RequestParam} 支持’application/json’，也同样支持multipart/form-data请求
     * 区别
     * 1、当请求方法的请求参数类型不是String 或 MultipartFile / Part时，而是复杂的请求域时，
     * @RequestParam 依赖Converter or PropertyEditor进行数据解析，
     * RequestPart参考 ‘Content-Type’ header，依赖HttpMessageConverters 进行数据解析
     * 2、当请求为multipart/form-data时，
     * @RequestParam只能接收String类型的name-value值，
     * @RequestParam 依赖Converter or PropertyEditor进行数据解析，
     * @RequestPart可以接收复杂的请求域（像json、xml）
     * @RequestPart参考'Content-Type' header，依赖HttpMessageConverters进行数据解析
     */
    @PostMapping("/upload/file2")
    public void uploadFile2(MultipartFile[] files, @RequestPart UserRequest userRequest,@RequestPart List<Integer> intList, String permission) {
        for (MultipartFile file : files) {
            System.out.println(file.getOriginalFilename());
        }
        System.out.println(intList);
        System.out.println(userRequest);
        System.out.println(permission);
    }

    @PostMapping("/noUpload")
    public void upload(@RequestBody UserRequest userRequest, String permission) {
        System.out.println(userRequest.toString());
        System.out.println(permission);
    }

    @PostMapping("/upload1")
    public void upload1(MultipartFile[] files, @RequestParam List<Integer> userRequest) {
        for (MultipartFile file : files) {
            System.out.println(file.getOriginalFilename());
        }
    }

    @PostMapping("/upload2")
    public void upload(FileUploadVO filesVo) {
        for (MultipartFile file : filesVo.getFiles()) {
            System.out.println(file.getOriginalFilename());
        }
        System.out.println(filesVo.getUserRequest());
    }
}
