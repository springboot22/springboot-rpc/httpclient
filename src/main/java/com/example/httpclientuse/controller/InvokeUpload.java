package com.example.httpclientuse.controller;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

@RestController
public class InvokeUpload {

//    @GetMapping("/invoke")
//    public void invokeUploadFile() throws URISyntaxException, IOException {
//        CloseableHttpClient defaultHttpClient = HttpClients.createDefault();
//
//        HttpPost httpPost = new HttpPost();
//        httpPost.setURI(new URI("http://127.0.0.1:8080/upload/file2"));
//
//        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
//        File file = new File("./a.txt");
//        // type1 文件类型数据
//        builder.addBinaryBody("files", Files.newInputStream(file.toPath()), ContentType.MULTIPART_FORM_DATA, file.getName());
//
//        // type2 字符串类型数据
//        builder.addTextBody("permission", "aaaaPermission");
//
//        // type3 对象类型数据
//        UserRequest userRequest = new UserRequest();
//        userRequest.setId(111L);
//        // 此种方法和下面的得到的结果一样 builder.addBinaryBody("userRequest", JSONObject.toJSONBytes(userRequest));
//        builder.addPart("userRequest", new StringBody(JSONObject.toJSONString(userRequest), ContentType.APPLICATION_JSON));
//
//        // type4 数组类型数据
//        List<Integer> intList = Arrays.asList(1, 2, 3);
//        builder.addPart("intList", new StringBody(JSONObject.toJSONString(intList), ContentType.APPLICATION_JSON));
//
//
//        httpPost.setEntity(builder.build());
//
//        CloseableHttpResponse execute = defaultHttpClient.execute(httpPost);
//        HttpEntity entity = execute.getEntity();
//        String s = EntityUtils.toString(entity);
//        System.out.println(s);
//    }

//    @GetMapping("/invoke2")
//    public void invokeRequestBody() throws URISyntaxException, IOException {
//        CloseableHttpClient defaultHttpClient = HttpClients.createDefault();
//        HttpPost httpPost = new HttpPost();
//        httpPost.setURI(new URI("http://127.0.0.1:8080/noUpload"));
//        UserRequest userRequest = new UserRequest();
//        userRequest.setId(111L);
//        StringEntity stringEntity = new StringEntity(JSONObject.toJSONString(userRequest), ContentType.APPLICATION_JSON);
//        httpPost.setEntity(stringEntity);
//        httpPost.setHeader("ContentType", "application/json");
//        CloseableHttpResponse execute = defaultHttpClient.execute(httpPost);
//        HttpEntity entity = execute.getEntity();
//        String s = EntityUtils.toString(entity);
//        System.out.println(s);
//    }

    @GetMapping("/invoke3.x")
    public void invokeRequestBody() throws URISyntaxException, IOException {
        File f = new File("./a.txt");
        PostMethod filePost = new PostMethod("http://127.0.0.1:8080/upload/file1");
        Part[] parts = {
                new StringPart("permission", "value"),
                new FilePart("files", f),
                new FilePart("files",f)
        };
        filePost.setRequestEntity(
                new MultipartRequestEntity(parts, new HttpMethodParams())
        );
        filePost.setRequestHeader("ContentType","multipart/form-data; boundary="+Part.getBoundary());
        HttpClient client = new HttpClient();
        int status = client.executeMethod(filePost);
        System.out.println(status);
    }
}
