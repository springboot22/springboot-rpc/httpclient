package com.example.httpclientuse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HttpClientUseApplication {

    public static void main(String[] args) {
        SpringApplication.run(HttpClientUseApplication.class, args);
    }

}
