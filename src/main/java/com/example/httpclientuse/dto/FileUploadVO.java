package com.example.httpclientuse.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
@Data
public class FileUploadVO {
    MultipartFile[] files;
    UserRequest userRequest;
}
